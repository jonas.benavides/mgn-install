pipeline {
    agent any

    parameters {
        string(name: 'REMOTE_SERVER', defaultValue: '', description: 'Digite o nome ou endereço IP do servidor remoto')
        choice(name: 'AWS_REGION', choices: ['us-east-1', 'us-west-2'], description: 'Selecione a região AWS')
        string(name: 'AWS_CREDENTIALS_ID', defaultValue: 'YOUR_AWS_CREDENTIALS_ID', description: 'ID das credenciais AWS no Jenkins')
    }

    stages {
        stage('Checkout') {
            steps {
                script {
                    checkout scm
                }
            }
        }

        stage('Install MGN AWS Agent on Remote Machine') {
            agent {
                label 'ssh-agent'
            }
            steps {
                script {
                    def remoteServer = params.REMOTE_SERVER
                    def awsRegion = params.AWS_REGION
                    def awsCredentialsId = params.AWS_CREDENTIALS_ID
                    def installerUrl = 'https://aws-application-migration-service-us-east-1.s3.us-east-1.amazonaws.com/latest/linux/aws-replication-installer-init'

                    // Validação do nome do servidor remoto
                    if (remoteServer.empty) {
                        error('O nome do servidor remoto não pode ser vazio.')
                    }

                    // Comandos para instalar o agente MGN AWS na máquina remota via SSH com credenciais do Jenkins
                    withCredentials([string(credentialsId: awsCredentialsId, variable: 'AWS_ACCESS_KEY_ID'),
                                     string(credentialsId: awsCredentialsId, variable: 'AWS_SECRET_ACCESS_KEY'),
                                     string(credentialsId: 'AWS_REGION', variable: 'AWS_REGION')]) {

                        sshagent(['ssh_key']) {
                            // Execute comandos SSH aqui
                            sh "ssh -o StrictHostKeyChecking=no benavides@${remoteServer} 'sudo wget -O ./aws-replication-installer-init ${installerUrl}'"
                            sh "ssh -o StrictHostKeyChecking=no benavides@${remoteServer} 'sudo chmod +x aws-replication-installer-init; sudo ./aws-replication-installer-init --region ${awsRegion}'"
                        }
                    }
                }
            }
        }
    }

    post {
        always {
            // Limpar ambiente após a execução do pipeline
            cleanWs()
        }
    }
}
