#!/bin/bash

# Jenkins parameters
REMOTE_HOST="$1"
AWS_REGION="$2"
SCRIPT_PATH="/path/to/your_script.sh"

# Function to install MGN agent on a Linux source server
install_mgn_agent_linux() {
    remote_user="your_remote_user"  # Replace with your actual remote user

    # Download AWS Replication Agent installer script
    ssh "$remote_user@$REMOTE_HOST" "wget -O ./aws-replication-installer-init.py https://aws-application-migration-service-$AWS_REGION.s3.$AWS_REGION.amazonaws.com/latest/linux/aws-replication-installer-init.py"

    # Commands to check requirements and run the installer script
    ssh "$remote_user@$REMOTE_HOST" << EOF
        # Check if Python is installed
        python_version=\$(python --version 2>&1)
        if [[ \$? -ne 0 ]]; then
            echo "Python is not installed. Please install Python 2 (2.4 or above) or Python 3 (3.0 or above)."
            exit 1
        fi

        # Check disk space on the root directory
        root_disk_space=\$(df -h / | awk 'NR==2 {print \$4}')
        if [[ \$(echo "\$root_disk_space < 2.0" | bc) -eq 1 ]]; then
            echo "Insufficient disk space on the root directory. At least 2 GB required."
            exit 1
        fi

        # Check free RAM
        free_ram=\$(free -m | awk 'NR==2 {print \$4}')
        if [[ \$(echo "\$free_ram < 300" | bc) -eq 1 ]]; then
            echo "Insufficient free RAM. At least 300 MB required."
            exit 1
        fi

        # Add more checks for other requirements as needed...

        # Run the installer script
        python ./aws-replication-installer-init.py
EOF
}

# Example usage
install_mgn_agent_linux "$REMOTE_HOST"
